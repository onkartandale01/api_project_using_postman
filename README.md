# API_Project_Using_Postman


## Introduction :-

By using postman tool, automating different REST_API like POST,GET,PUT,PATCH,DELETE.

## Content :-

Project content as follows,
1.README.md file.
2.Collection of different REST_API.
3.Collection of REST_API to do Data_driven testing and Request chaining. 
4.Postman Environment file.
5.Newman_command file.

## Description :-
1.README.md file :-    In this file you'll get to know each and every thing about this project i.e., Introduction,Content,Discription etc.

2.Collection of different REST_API :-   In this Collection file there is different REST_API Request like POST,GET,PUT,PATCH,DELETE.
this request's are with all test scripts cover for response validation,status code validation etc,.

3.Collection of REST_API to do Data_driven testing and Request chaining :-  This Collection is made for specially Data_driven testing and retrying API till fix number of iteration.
- By using JSON & CSV data files we'll do the Data_driven testing in postman.
- In this collection we also provide the retrying API(Request_chaining) test script so by running this at collection runner level we can run/trigger this request multiple times(Without using Collection_runner).

4.Postman Environment :-    For running collection of different REST_API we create/set the Environment.In this Environment we set the all End_point/URL as variable and parametrized it at specific request method.


5.Newman_command file :-    This file is a text file, in which we provide the different Newman_command for running collection with Environment on CLI and generating reports(html & htmlextra).we can also export this reports in html format for further reference.
An added advantage for other team member is they dont need to set up postman tool for running this collection and generating reports.
